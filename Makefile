.PHONY: compile clean

compile:
	@find -type f -regextype posix-extended -not -regex '.*\.git.*' -regex '.*\.c' -execdir sh -c 'basename {} .c | xargs -I _ cc -ansi _.c -o _.out' \;
clean:
	@find -type f -executable -regextype posix-extended -not -regex '.*\.git.*' -regex '.*\.out' -execdir rm -v {} +
