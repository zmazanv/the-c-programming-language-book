# The C Programming Language

This includes everything I wrote when going through the classic book
'The C Programming Language' by Brian W. Kernighan and Dennis M. Ritchie.

**This covered specifically the original edition of the book and not the
2nd edition, hence the legacy style of the code here.**
